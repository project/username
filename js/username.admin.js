/**
 * @file
 * Username behaviors.
 */

(function ($, window, Drupal, once) {
  /**
   * Provide the summary information for the username settings vertical tabs.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the username settings summaries.
   */
  Drupal.behaviors.usernameSettingsSummary = {
    attach() {
      // The drupalSetSummary method required for this behavior is not available
      // on the Username administration page, so we need to make sure this
      // behavior is processed only if drupalSetSummary is defined.
      if (typeof $.fn.drupalSetSummary === 'undefined') {
        return;
      }

      /**
       * Create a summary for checkboxes in the provided context.
       *
       * @param {HTMLDocument|HTMLElement} context
       *   A context where one would find checkboxes to summarize.
       *
       * @return {string}
       *   A string with the summary.
       */
      function overrideSummary(context) {
        const $checkbox = $(context).find(
          'input[type="checkbox"]:checked',
        );

        if (!$checkbox.length) {
          return Drupal.t('Not overridden');
        }

        return Drupal.t('Overridden to certain strings');
      }

      $(
        '[data-drupal-selector="edit-username-strings"], [data-drupal-selector="edit-password-strings"], [data-drupal-selector="edit-login-strings"], [data-drupal-selector="edit-register-strings"], [data-drupal-selector="edit-reset-strings"]',
      ).drupalSetSummary(overrideSummary);
    },
  };

  /**
   * Move a block in the blocks table between regions via select list.
   *
   * This behavior is dependent on the tableDrag behavior, since it uses the
   * objects initialized in that behavior to update the row.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the tableDrag behavior for blocks in block administration.
   */
  Drupal.behaviors.usernameForm = {
    attach(context, settings) {

      /**
       * Create a summary for checkboxes in the provided context.
       *
       * @param {HTMLDocument|HTMLElement} context
       *   A context where one would find checkboxes to summarize.
       *
       * @return {string}
       *   A string with the summary.
       */
      function getUsernameModes(context) {
        const values = [];
        const $modes = $(context).find(
          '#edit-modes input[type="checkbox"]:checked',
        );

        const il = $modes.length;
        for (let i = 0; i < il; i++) {
          values.push($($modes[i]).val());
        }

        return values;
      }

      /**
       * Create a summary for checkboxes in the provided context.
       *
       * @param {HTMLDocument|HTMLElement} context
       *   A context where one would find checkboxes to summarize.
       *
       * @return {string}
       *   A string with the summary.
       */
      function updateRestrictsUsername(usernameModes) {
        const $restricts = $('[data-drupal-selector="edit-restriction"]');
        if ($.inArray('name', usernameModes) > -1) {
          $restricts.parent().show();
        }
        else {
          $restricts.prop( "checked", false ).trigger('change');
          $restricts.parent().hide();
        }
      }

      /**
       * Create a summary for checkboxes in the provided context.
       *
       * @param {HTMLDocument|HTMLElement} context
       *   A context where one would find checkboxes to summarize.
       *
       * @return {string}
       *   A string with the summary.
       */
      function updateSelectiveUsername(usernameModes) {
        const $selective = $('[data-drupal-selector="edit-selective"]');
        const $primary = $('[data-drupal-selector="edit-primary"] option');

        if (usernameModes.length < 2) {
          $selective.prop( "checked", false ).trigger('change');
          $selective.attr("disabled", true);
        }
        else {
          let defaultUsername = $('[data-drupal-selector="edit-primary"]').find(":selected").val();
          $primary.each(function() {
            if ($.inArray($(this).val(), usernameModes) > -1) {
              $(this).show();
              if ($.inArray(defaultUsername, usernameModes) === -1) {
                $primary.parent().val($(this).val()).change();
                defaultUsername = $(this).val();
              }
            }
            else {
              $(this).hide();
            }
          });
          $selective.removeAttr("disabled");
        }
      }

      // Add the behavior to each region select list.
      $(once('usernames', '#edit-modes input[type="checkbox"]', context)).on(
        'change',
        function (event) {
          const usernameModes = getUsernameModes(context);
          updateSelectiveUsername(usernameModes);
          updateRestrictsUsername(usernameModes);
        },
      ).trigger('change');

    },
  };

})(jQuery, window, Drupal, once);
