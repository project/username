<?php

namespace Drupal\username_phone\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if the user's phone number is provided if required.
 *
 * The user phone field is NOT required if account originally had no phone set
 * and the user performing the edit has 'administer users' permission.
 * This allows users without phone number to be edited and deleted.
 *
 * @Constraint(
 *   id = "UsernamePhoneRequired",
 *   label = @Translation("Username phone required", context = "Validation")
 * )
 */
class UsernamePhoneRequired extends Constraint {

  /**
   * Violation message. Use the same message as FormValidator.
   *
   * Note that the name argument is not sanitized so that translators only have
   * one string to translate. The name is sanitized in self::validate().
   *
   * @var string
   */
  public $message = '@name field is required.';

}
