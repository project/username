<?php

namespace Drupal\username_phone\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Checks if the user's phone number is provided if required.
 *
 * The user phone field is NOT required if account originally had no phone set
 * and the user performing the edit has 'administer users' permission.
 * This allows users without phone number to be edited and deleted.
 */
class UsernamePhoneRequiredValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    /** @var \Drupal\user\UserInterface $account */
    $account = $items->getEntity();
    if (!isset($account)) {
      return;
    }

    // Load username configuration settings.
    $config = \Drupal::config('username.settings');
    $required = (isset($username['phone']) || $config->get('phone.verification')) && !(!$account->getPhone() && \Drupal::currentUser()->hasPermission('administer users'));

    if ($required && (!isset($items) || $items->isEmpty())) {
      $this->context->addViolation($constraint->message, ['@name' => $account->getFieldDefinition('phone')->getLabel()]);
    }
  }

}
