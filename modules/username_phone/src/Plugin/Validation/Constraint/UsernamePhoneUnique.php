<?php

namespace Drupal\username_phone\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldConstraint;

/**
 * Checks if a user's phone number is unique on the site.
 *
 * @Constraint(
 *   id = "UsernamePhoneUnique",
 *   label = @Translation("User phone unique", context = "Validation")
 * )
 */
class UsernamePhoneUnique extends UniqueFieldConstraint {

  /**
   * Error message shown when the phone number is not unique.
   *
   * @var string
   */
  public $message = 'The phone number %value is already taken.';

}
