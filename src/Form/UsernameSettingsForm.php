<?php

namespace Drupal\username\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure username settings for this site.
 */
class UsernameSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new UsernameSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, ModuleHandlerInterface $module_handler, RendererInterface $renderer) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('module_handler'),
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'username_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'username.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('username.settings');

    // Settings for type of username field.
    $usernames = username_options();
    $usernames['name'] = isset($usernames['name']) ? $this->t('Name (Default)') : '';
    $form['username_settings'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Username settings'),
      '#open'   => TRUE,
      '#weight' => -9,
    ];
    $form['username_settings']['modes'] = [
      '#type'          => 'checkboxes',
      '#options'       => $usernames,
      '#title'         => $this->t('Username mode'),
      '#description'   => $this->t('Select the username mode(s) you want to use for the registration form and login on the site.'),
      '#default_value' => $config->get('username') ?? ['name'],
      '#required'      => TRUE,
    ];

    // Generate username automatically in disabled name field.
    $form['username_settings']['auto'] = [
      '#type'          => 'radios',
      '#options'       => username_generate_options(),
      '#title'         => $this->t('Generates a username automatically using'),
      '#description'   => $this->t('When you disable the default core username field, the name column in the user data table still exists and cannot be left blank and must be populated with one of the options above.'),
      '#default_value' => $config->get('auto'),
      '#states' => [
        'visible' => [
          ':input[name="modes[name]"]' => ['checked' => FALSE],
        ],
      ],
    ];
    $form['username_settings']['auto_generate'] = [
      '#type' => 'fieldset',
      '#states' => [
        'visible' => [
          ':input[name="auto"]' => ['value' => 'token'],
        ],
      ],
    ];
    $token_tree = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['user'],
      '#global_types' => FALSE,
    ];
    $token_tree_link = $this->renderer->render($token_tree);
    $form['username_settings']['auto_generate']['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username pattern'),
      '#description' => $this->t('Enter the pattern used to construct a username with user tokens. @browse_tokens_link', [
        '@browse_tokens_link' => $token_tree_link,
      ]),
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['user'],
      '#min_tokens' => 1,
      '#states' => [
        'required' => [
          ':input[name="auto"]' => ['value' => 'token'],
        ],
        'visible' => [
          ':input[name="auto"]' => ['value' => 'token'],
        ],
      ],
      '#default_value' => $config->get('token.pattern'),
    ];
    $form['username_settings']['auto_generate']['convert_lowercase'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Convert to lowercase'),
      '#default_value' => $config->get('token.lowercase'),
    ];
    $form['username_settings']['auto_generate']['remove_whitespace'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove whitespace'),
      '#default_value' => $config->get('token.whitespace'),
    ];
    $form['username_settings']['auto_generate']['remove_punctuation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove punctuation'),
      '#default_value' => $config->get('token.punctuation'),
    ];

    // Selectable username for login page in multiple username modes.
    $form['username_settings']['selective'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Selectable username on the login page'),
      '#description'   => $this->t('The username option for login will be split and users can choose how they want to login. Enabling this option helps match the username field to the username type for better control over validation.'),
      '#default_value' => $config->get('selective.enabled'),
    ];

    // Primary username for default selected username.
    $form['username_settings']['primary'] = [
      '#type'          => 'select',
      '#options'       => $usernames,
      '#title'         => $this->t('Primary username'),
      '#description'   => $this->t('Choose which username you want to be the primary to generate core username based on email or phone number. When you use email and phone number as username same time without core username, you must use one of them as your main username.'),
      '#default_value' => $config->get('selective.primary'),
      '#states' => [
        'visible' => [
          ':input[name="selective"]'  => ['checked' => TRUE],
        ],
      ],
    ];
    $form['username_settings']['switch'] = [
      '#type'          => 'select',
      '#options'       => username_switch_options(),
      '#title'         => $this->t('Switch style'),
      '#description'   => $this->t('Choose the switch style to be used when both email and phone number are allowed as usernames.'),
      '#default_value' => $config->get('selective.switch'),
      '#states' => [
        'visible' => [
          ':input[name="selective"]'  => ['checked' => TRUE],
        ],
      ],
    ];

    // Override user display name instead of display username.
    $form['username_settings']['display'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Override user display name'),
      '#description'   => $this->t('Provides a dynamic replacement for users display name.<br>Note: It is <strong>highly recommended</strong> if name (default core username) is <em>not selected</em>, because it uses name for the users display name.'),
      '#default_value' => $config->get('display'),
    ];
    $form['username_settings']['replace'] = [
      '#type'          => 'radios',
      '#options'       => username_display_options(),
      '#title'         => $this->t('Display name'),
      '#description'   => $this->t('Allows displaying the full name (real-name or nickname) instead of the username on the profile page, comments, posts, etc.'),
      '#default_value' => $config->get('replace'),
      '#states' => [
        'visible' => [
          ':input[name="display"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['username_settings']['pattern'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="display"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['username_settings']['pattern']['custom'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom display name'),
      '#description' => $this->t('Enter the pattern used to construct a display name for all users.<br>Note: all current display names will be deleted and the list in the database will be rebuilt as needed.'),
      '#element_validate' => ['token_element_validate'],
      '#token_types' => ['user'],
      '#min_tokens' => 1,
      '#states' => [
        'required' => [
          ':input[name="replace"]' => ['value' => 'custom'],
        ],
        'visible' => [
          ':input[name="replace"]' => ['value' => 'custom'],
        ],
      ],
      '#default_value' => $config->get('custom'),
    ];
    if ($this->moduleHandler->moduleExists('token')) {
      // Add the token tree UI.
      $form['username_settings']['pattern']['token_help'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['user'],
        '#global_types' => FALSE,
      ];
    }

    // Username prevention and restrictions.
    $form['username_restricts'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Prevention and restrictions'),
      '#open'   => TRUE,
      '#weight' => -8,
    ];
    $form['username_restricts']['prevention'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable enumeration prevention'),
      '#description' => $this->t('Prevents disclosure of existing usernames by changing 403 (Access Denied) to 404 (Not Found) in user profiles and hiding messages such as "The user has not been activated yet or is blocked." in the reset password form.'),
      '#default_value' => $config->get('prevention'),
    ];
    $form['username_restricts']['restriction'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable input restriction'),
      '#description' => $this->t('Restricts the username input field to use only special letters or characters. This validation includes client-side and server-side, which helps the security of your form to prevent spam and bots.'),
      '#default_value' => $config->get('restriction.enabled'),
    ];
    $form['username_restricts']['masks'] = [
      '#type'   => 'fieldset',
      '#title'  => $this->t('Username input mask'),
      '#states'        => [
        'visible' => [
          ':input[name="restriction"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['username_restricts']['masks']['lowercase'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow lowercase letters (a-z)'),
      '#default_value' => $config->get('restriction.mask.lowercase'),
    ];
    $form['username_restricts']['masks']['uppercase'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow uppercase letters (A-Z)'),
      '#default_value' => $config->get('restriction.mask.uppercase'),
    ];
    $form['username_restricts']['masks']['numeric'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow numbers (0-9)'),
      '#default_value' => $config->get('restriction.mask.numeric'),
    ];
    $form['username_restricts']['masks']['space'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow space'),
      '#default_value' => $config->get('restriction.mask.space'),
    ];
    $form['username_restricts']['masks']['period'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow period (.)'),
      '#default_value' => $config->get('restriction.mask.period'),
    ];
    $form['username_restricts']['masks']['hyphen'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow hyphen (-)'),
      '#default_value' => $config->get('restriction.mask.hyphen'),
    ];
    $form['username_restricts']['masks']['underscore'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow underscore (_)'),
      '#default_value' => $config->get('restriction.mask.underscore'),
    ];
    $form['username_restricts']['masks']['apostrophe'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow apostrophe (')"),
      '#default_value' => $config->get('restriction.mask.apostrophe'),
    ];
    $form['username_restricts']['masks']['sign'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow At sign (@)"),
      '#default_value' => $config->get('restriction.mask.sign'),
    ];
    $form['username_restricts']['masks']['minlength'] = [
      '#type' => 'number',
      '#min' => 6,
      '#max' => 60,
      '#title' => $this->t("Min length"),
      '#default_value' => $config->get('restriction.mask.minlength'),
      '#required' => TRUE,
    ];
    $form['username_restricts']['masks']['maxlength'] = [
      '#type' => 'number',
      '#min' => 6,
      '#max' => 60,
      '#title' => $this->t("Max length"),
      '#default_value' => $config->get('restriction.mask.maxlength'),
      '#required' => TRUE,
    ];

    // Additional features for user forms.
    $form['additional_options'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Additional options'),
      '#open'   => TRUE,
      '#weight' => 9,
    ];
    $form['additional_options']['autofocus'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Autofocus for username'),
      '#description'   => $this->t('Enable this option for browsers to automatically set the focus in the username field.'),
      '#default_value' => $config->get('options.autofocus'),
    ];
    $form['additional_options']['autocomplete'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Clear browser autocomplete'),
      '#description'   => $this->t('Enable this option to clear saved username and password fields in user forms.'),
      '#default_value' => $config->get('options.autocomplete'),
    ];
    $form['additional_options']['emailmask'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Email force lowercase'),
      '#description'   => $this->t('Enable this option to force email input to be lowercase.'),
      '#default_value' => $config->get('options.emailmask'),
    ];
    $form['additional_options']['capslock'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Caps-Lock warning'),
      '#description'   => $this->t('Enable this option to display a caps lock warning message for the password field.'),
      '#default_value' => $config->get('options.capslock'),
    ];
    $form['additional_options']['showhide'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Password show / hide'),
      '#description'   => $this->t('Enable this option to add a small eye icon to the password field for showing and hiding the password.'),
      '#default_value' => $config->get('options.showhide'),
    ];
    $form['additional_options']['generate'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Password generation'),
      '#description'   => $this->t('Enable this option to add secure password generation in the user registration form.'),
      '#default_value' => $config->get('options.generate'),
    ];

    // Vertical tabs for variability.
    $form['variability'] = [
      '#type'   => 'vertical_tabs',
      '#title'  => $this->t('Label and Placeholder'),
      '#weight' => 99,
    ];

    // Username strings override.
    $form['username_strings'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Username'),
      '#description' => $this->t('When selectable username for multiple username modes is enabled, you can set the title, placeholder, and description of each username by dividing them by double percent sign "%%" in their order.'),
      '#attributes' => ['class' => ['details--settings', 'b-tooltip']],
      '#group'      => 'variability',
      '#open'       => TRUE,
    ];
    $form['username_strings']['username_override'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Override username strings'),
      '#description'   => $this->t('Enable to change the title, placeholder, and description with your desired settings.'),
      '#default_value' => $config->get('override.username'),
    ];
    $form['username_strings']['username_wrapper'] = [
      '#type'   => 'container',
      '#states' => [
        'enabled'  => [
          ':input[name="username_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['username_strings']['username_wrapper']['username_label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Username label'),
      '#description'   => $this->t('Leave blank if you don’t want to have a field title; in this case, you have to set a placeholder instead. Override the field title with your desired settings, e.g., "Username or email address" for non-selectable and "Username %% Email address" for selectable usernames.'),
      '#default_value' => $config->get('label.username') ?? '',
    ];
    $form['username_strings']['username_wrapper']['username_placeholder'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Username placeholder'),
      '#description'   => $this->t('Leave blank if you don’t want to set a placeholder. If you have left the title blank, you must fill in the placeholder instead. e.g., "Your username / email address" for non-selectable, "Your username %% yourname@example.com" for selectable usernames.'),
      '#default_value' => $config->get('placeholder.username') ?? '',
      '#states' => [
        'required' => [
          ':input[name="username_override"]' => ['checked' => TRUE],
          ':input[name="username_label"]' => ['value' => ''],
        ],
      ],
    ];
    $form['username_strings']['username_wrapper']['username_description'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Username description'),
      '#description'   => $this->t('Leave blank if you don’t want to have a field description. Override the field description with your desired settings, e.g., "Enter your Username or email address." for non-selectable and "Enter your registered username. %% Enter your valid email address" for selectable usernames.'),
      '#default_value' => $config->get('description.username') ?? '',
    ];

    // Password strings override.
    $form['password_strings'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Password'),
      '#attributes' => ['class' => ['details--settings', 'b-tooltip']],
      '#group'      => 'variability',
    ];
    $form['password_strings']['password_override'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Override password strings'),
      '#description'   => $this->t('Enable to change the title, placeholder, and description with your desired settings.'),
      '#default_value' => $config->get('override.password'),
    ];
    $form['password_strings']['password_wrapper'] = [
      '#type'   => 'container',
      '#states' => [
        'enabled'  => [
          ':input[name="password_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['password_strings']['password_wrapper']['password_label'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Password label'),
      '#description'   => $this->t('Override the field title with your desired settings. Leave blank if you don’t want to have a field title; in this case, you have to set a placeholder instead.'),
      '#default_value' => $config->get('label.password') ?? '',
    ];
    $form['password_strings']['password_wrapper']['password_placeholder'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Password placeholder'),
      '#description'   => $this->t('Leave blank if you don’t want to set a placeholder. If you have left the title blank, you must fill in the placeholder instead.'),
      '#default_value' => $config->get('placeholder.password') ?? '',
      '#states' => [
        'required' => [
          ':input[name="password_override"]' => ['checked' => TRUE],
          ':input[name="password_label"]' => ['value' => ''],
        ],
      ],
    ];
    $form['password_strings']['password_wrapper']['password_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password description'),
      '#description' => $this->t('Override the field description with your desired settings. Leave blank if you don’t want to have a field description.'),
      '#default_value' => $config->get('description.password') ?? '',
    ];

    // Login page strings override.
    $form['login_strings'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Login page'),
      '#attributes' => ['class' => ['details--settings', 'b-tooltip']],
      '#group'      => 'variability',
    ];
    $form['login_strings']['login_override'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Override login page strings'),
      '#description'   => $this->t('Enable to change the title, button label, and messages with your desired settings.'),
      '#default_value' => $config->get('override.login'),
    ];
    $form['login_strings']['login_wrapper'] = [
      '#type'   => 'container',
      '#states' => [
        'enabled'  => [
          ':input[name="login_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['login_strings']['login_wrapper']['login_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Login page title'),
      '#description'   => $this->t('Override the page title with your desired settings.'),
      '#default_value' => $config->get('title.login') ?? $this->t('Username or email address'),
      '#states' => [
        'required' => [
          ':input[name="login_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['login_strings']['login_wrapper']['login_button'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Login button label'),
      '#description'   => $this->t('Override the button label with your desired settings.'),
      '#default_value' => $config->get('button.login') ?? '',
      '#states' => [
        'required' => [
          ':input[name="login_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['login_strings']['login_wrapper']['login_message'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Login message'),
      '#description'   => $this->t('Override the page description with your desired settings.'),
      '#default_value' => $config->get('message.login') ?? '',
    ];

    // Register page strings override.
    $form['register_strings'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Register page'),
      '#attributes' => ['class' => ['details--settings', 'b-tooltip']],
      '#group'      => 'variability',
    ];
    $form['register_strings']['register_override'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Override register page strings'),
      '#description'   => $this->t('Enable to change the title, button label, and messages with your desired settings.'),
      '#default_value' => $config->get('override.register'),
    ];
    $form['register_strings']['register_wrapper'] = [
      '#type'   => 'container',
      '#states' => [
        'enabled'  => [
          ':input[name="register_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['register_strings']['register_wrapper']['register_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Register page title'),
      '#description'   => $this->t('Override the page title with your desired settings.'),
      '#default_value' => $config->get('title.register') ?? $this->t('Username or email address'),
      '#states' => [
        'required' => [
          ':input[name="register_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['register_strings']['register_wrapper']['register_button'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Register button label'),
      '#description'   => $this->t('Override the button label with your desired settings.'),
      '#default_value' => $config->get('button.register') ?? '',
      '#states' => [
        'required' => [
          ':input[name="register_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['register_strings']['register_wrapper']['register_message'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Register message'),
      '#description'   => $this->t('Override the page description with your desired settings.'),
      '#default_value' => $config->get('message.register') ?? '',
    ];

    // Reset password page strings override.
    $form['reset_strings'] = [
      '#type'       => 'details',
      '#title'      => $this->t('Reset password page'),
      '#attributes' => ['class' => ['details--settings', 'b-tooltip']],
      '#group'      => 'variability',
    ];
    $form['reset_strings']['reset_override'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Override reset password strings'),
      '#description'   => $this->t('Enable to change the title, button label, and messages with your desired settings.'),
      '#default_value' => $config->get('override.reset'),
    ];
    $form['reset_strings']['reset_wrapper'] = [
      '#type'   => 'container',
      '#states' => [
        'enabled'  => [
          ':input[name="reset_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['reset_strings']['reset_wrapper']['reset_title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Reset password page title'),
      '#description'   => $this->t('Override the page description with your desired settings.'),
      '#default_value' => $config->get('title.reset') ?? $this->t('Username or email address'),
      '#states' => [
        'required' => [
          ':input[name="reset_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['reset_strings']['reset_wrapper']['reset_button'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Reset button label'),
      '#description'   => $this->t('Override the button label with your desired settings.'),
      '#default_value' => $config->get('button.reset') ?? '',
      '#states' => [
        'required' => [
          ':input[name="reset_override"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['reset_strings']['reset_wrapper']['reset_message'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Reset password message'),
      '#description'   => $this->t('Override the page description with your desired settings.'),
      '#default_value' => $config->get('message.reset') ?? '',
    ];

    // The username settings library.
    $form['#attached']['library'][] = 'username/username.admin';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Check if the username mode 'name' is selected along
    // with the restriction enabled and ensure at least one
    // of uppercase, lowercase or numeric is selected.
    $username = array_filter($values['modes']);
    if (isset($username['name']) && $values['restriction'] && !($values['lowercase'] || $values['uppercase'] || $values['numeric'])) {
      $form_state->setErrorByName('restriction', $this->t('To restrict the username field, at least one of uppercase or lowercase letters or numbers must be selected.'));
    }

    // Ensure that if username strings override is enabled,
    // either label or placeholder must be filled.
    if ($values['username_override'] && empty(trim($values['username_label'])) && empty(trim($values['username_placeholder']))) {
      $form_state->setErrorByName('username_label', $this->t('The username field title and placeholder cannot be empty at the same time. At least one of them must have a value.'));
      $form_state->setErrorByName('username_placeholder', '');
    }

    // Ensure that if password strings override is enabled,
    // either label or placeholder must be filled.
    if ($values['password_override'] && empty(trim($values['password_label'])) && empty(trim($values['password_placeholder']))) {
      $form_state->setErrorByName('password_label', $this->t('The password field title and placeholder cannot be empty at the same time. At least one of them must have a value.'));
      $form_state->setErrorByName('password_placeholder', '');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $username = array_filter($values['modes']);
    $username_auto = !isset($username['name']) ? $values['auto'] : 'hash';
    $username_restriction = isset($username['name']) ? $values['restriction'] : FALSE;

    // Save the updated username settings.
    $this->config('username.settings')
      ->set('username', $username)
      ->set('auto', $username_auto)
      ->set('token.pattern', $values['token'])
      ->set('token.lowercase', $values['convert_lowercase'])
      ->set('token.whitespace', $values['remove_whitespace'])
      ->set('token.punctuation', $values['remove_punctuation'])
      ->set('selective.enabled', $values['selective'] && count($username) > 1)
      ->set('selective.primary', $values['primary'])
      ->set('selective.switch', $values['switch'])
      ->set('display', $values['display'])
      ->set('replace', $values['replace'])
      ->set('custom', $values['custom'])
      ->set('prevention', $values['prevention'])
      ->set('restriction.enabled', $username_restriction)
      ->set('restriction.mask.lowercase', $values['lowercase'])
      ->set('restriction.mask.uppercase', $values['uppercase'])
      ->set('restriction.mask.numeric', $values['numeric'])
      ->set('restriction.mask.space', $values['space'])
      ->set('restriction.mask.period', $values['period'])
      ->set('restriction.mask.hyphen', $values['hyphen'])
      ->set('restriction.mask.apostrophe', $values['apostrophe'])
      ->set('restriction.mask.underscore', $values['underscore'])
      ->set('restriction.mask.sign', $values['sign'])
      ->set('restriction.mask.minlength', $values['minlength'])
      ->set('restriction.mask.maxlength', $values['maxlength'])
      ->set('options.autofocus', $values['autofocus'])
      ->set('options.autocomplete', $values['autocomplete'])
      ->set('options.emailmask', $values['emailmask'])
      ->set('options.capslock', $values['capslock'])
      ->set('options.showhide', $values['showhide'])
      ->set('options.generate', $values['generate'])
      ->set('override.username', $values['username_override'])
      ->set('override.password', $values['password_override'])
      ->set('override.login', $values['login_override'])
      ->set('override.register', $values['register_override'])
      ->set('override.reset', $values['reset_override'])
      ->set('label.username', trim($values['username_label']))
      ->set('label.password', trim($values['password_label']))
      ->set('placeholder.username', trim($values['username_placeholder']))
      ->set('placeholder.password', trim($values['password_placeholder']))
      ->set('description.username', trim($values['username_description']))
      ->set('description.password', trim($values['password_description']))
      ->set('title.login', trim($values['login_title']))
      ->set('title.register', trim($values['register_title']))
      ->set('title.reset', trim($values['reset_title']))
      ->set('button.login', trim($values['login_button']))
      ->set('button.register', trim($values['register_button']))
      ->set('button.reset', trim($values['reset_button']))
      ->set('message.login', trim($values['login_message']))
      ->set('message.register', trim($values['register_message']))
      ->set('message.reset', trim($values['reset_message']))
      ->save();

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
