<?php

namespace Drupal\username\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an action to update user display names.
 *
 * @Action(
 *   id = "username_update_displayname_action",
 *   label = @Translation("Update display name"),
 *   type = "user"
 * )
 */
class UsernameUpdateDisplayName extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($account = NULL) {
    if ($account) {
      username_display_name_update($account);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // Ensure user has permission to edit status and update account.
    /** @var \Drupal\user\UserInterface $object */
    $access = $object->status->access('edit', $account, TRUE)
      ->andIf($object->access('update', $account, TRUE));

    return $return_as_object ? $access : $access->isAllowed();
  }

}
