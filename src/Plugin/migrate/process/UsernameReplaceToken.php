<?php

namespace Drupal\username\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Replaces tokens in the source value during migration.
 *
 * @MigrateProcessPlugin(
 *   id = "username_replace_token",
 *   handle_multiples = TRUE
 * )
 */
class UsernameReplaceToken extends ProcessPluginBase {

  /**
   * Transforms the source value by replacing specific tokens.
   *
   * @param mixed $value
   *   The source value.
   * @param \Drupal\migrate\MigrateExecutableInterface $migrate_executable
   *   The migration executable.
   * @param \Drupal\migrate\Row $row
   *   The current row.
   * @param string $destination_property
   *   The destination property.
   *
   * @return mixed
   *   The transformed value.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    [$displayname_pattern] = $value;

    // Replace old D7 realname tokens with D8 core tokens.
    // Tokens to replace:
    // - [user:name-raw] -> [user:account-name]
    // - [current-user:name-raw] -> [current-user:account-name].
    return str_ireplace(':name-raw]', ':account-name]', $displayname_pattern);
  }

}
