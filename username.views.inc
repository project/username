<?php

/**
 * @file
 * Provides Views integration for the Username module.
 */

/**
 * Implements hook_views_data().
 */
function username_views_data() {
  $data['username']['table']['group'] = t('Username');
  $data['username']['table']['join'] = [
    'users_field_data' => [
      'left_field' => 'uid',
      'field' => 'uid',
    ],
  ];

  $data['username']['displayname'] = [
    'title' => t('Display name'),
    'help' => t("The user's display name."),
    'field' => [
      'id' => 'standard',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
      'title' => t('Name'),
      'help' => t("The user's display name; this filter does not check if the user exists and allows partial matching. It does not utilize autocomplete."),
    ],
  ];

  return $data;
}
